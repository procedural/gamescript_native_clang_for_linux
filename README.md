# Game Script Native Clang for Linux

## Features:

* Script code autocomplete with F1 key.

## Install:

* Copy all the files from `GameScriptNative` folder of this repo to `/opt/GameScriptNative`.
* Install dependencies:
```sh
sudo apt install clang
```
