#!/bin/bash

#========#
# COLORS #
#========#

red=""
gray=""
blue=""
pink=""
cyan=""
white=""
black=""
green=""
brown=""
yellow=""
purple=""
normal=""
bg_black=""
bg_white=""
light_red=""
light_gray=""
light_blue=""
light_cyan=""
light_green=""

#=========#
# OPTIONS #
#=========#

find='`'
return_color=${light_cyan}
argument_color=${light_green}
default_argument_color=${light_red}

#================#
# IMPLEMENTATION #
#================#

IFS='%'
line=$(grep -n ${find} "${1}" | grep -o "^[0-9]*")
if [[ -z ${line} ]]; then exit 1; fi
body=$(sed "${line}q; d" "${1}" | cut -d ${find} -f1)
tail=$(echo "${body}" | rev | \
cut -d '{' -f1 | \
cut -d '}' -f1 | \
cut -d '[' -f1 | \
cut -d ']' -f1 | \
cut -d '#' -f1 | \
cut -d '(' -f1 | \
cut -d ')' -f1 | \
cut -d ';' -f1 | \
cut -d ':' -f1 | \
cut -d '.' -f1 | \
cut -d '+' -f1 | \
cut -d '-' -f1 | \
cut -d '*' -f1 | \
cut -d '/' -f1 | \
cut -d '%' -f1 | \
cut -d '^' -f1 | \
cut -d '&' -f1 | \
cut -d '|' -f1 | \
cut -d '~' -f1 | \
cut -d '!' -f1 | \
cut -d '=' -f1 | \
cut -d '<' -f1 | \
cut -d '>' -f1 | \
cut -d ',' -f1 | \
cut -d ' ' -f1 | rev)
column=$((${#body} - ${#tail} + 1))
format="s_#\]_#\] _1; s_\[#_${return_color}_g; s_#\]_${normal}_g; s_<#_${argument_color}_g; s_#>_${normal}_g; s_{#, _,${default_argument_color} \$_g; s_#}_${normal}_g"
clang=$(clang "${@}" -fcolor-diagnostics -fsyntax-only -Xclang -code-completion-macros -Xclang -code-completion-patterns -Xclang -code-completion-brief-comments -Xclang -code-completion-at="${1}":${line}:${column} \
| sed -z "s_\n__g; s_OVERLOAD: _\n${normal}OVERLOAD: _g; s_COMPLETION: _\n${normal}COMPLETION: _g" | sed "${format}; /^$/d")
overload=$(echo "${clang}" | grep "OVERLOAD: ")
complete=$(echo "${clang}" | sed "/COMPLETION: Pattern : /d" | grep "COMPLETION: ${tail}")
patterns=$(echo "${clang}" | grep "COMPLETION: Pattern : "   | grep "${tail}")
if [[ ! -z ${overload} ]]; then echo -e "\n${overload}"; fi
if [[ ! -z ${complete} ]]; then echo -e "\n${complete}"; fi
if [[ ! -z ${patterns} ]]; then echo -e "\n${patterns}"; fi

