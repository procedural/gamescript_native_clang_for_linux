#!/usr/bin/env bash

cd "$(dirname -- "$(readlink -fn -- "$0")")"

rm -f /opt/GameScriptNative/f1.cpp
cp /opt/GameScriptNative/f1.script /opt/GameScriptNative/f1.cpp
sed -i $'s/\t/ /g' /opt/GameScriptNative/f1.cpp
/opt/GameScriptNative/clang_complete.sh /opt/GameScriptNative/f1.cpp -include /opt/GameScriptNative/libgamescriptnative.cpp

